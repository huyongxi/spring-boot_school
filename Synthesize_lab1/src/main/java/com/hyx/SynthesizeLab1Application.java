package com.hyx;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.hyx.mapper")
public class SynthesizeLab1Application {

    public static void main(String[] args) {
        SpringApplication.run(SynthesizeLab1Application.class, args);
    }

}
