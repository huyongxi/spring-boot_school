package com.hyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hyx.pojo.Person;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface personMapper extends BaseMapper<Person> {
}
