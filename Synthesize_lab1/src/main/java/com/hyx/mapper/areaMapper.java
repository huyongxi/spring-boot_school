package com.hyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hyx.pojo.Area;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface areaMapper extends BaseMapper<Area> {
}
