package com.hyx.controller;

import com.hyx.pojo.Person;
import com.hyx.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class loginController {

    @Autowired
    LoginService service;

    @RequestMapping("/")
    public String index() {
        return "index";
    }

    @RequestMapping("/login")
    public String login(Person person) {
        service.addMessage(person);
        if (person.getJkm().equals("y")) {
            return "redirect:/rget?personId="+person.getUid();
        }
        return "redirect:/err";
    }

    @RequestMapping("/rget")
    public String rget(Integer personId,Model model) {
        String message = service.getMessage(personId);
        model.addAttribute("message",message);
        return "rget";
    }

    @RequestMapping("/err")
    public String err() {
        return "err";
    }

}
