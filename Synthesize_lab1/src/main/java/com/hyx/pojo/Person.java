package com.hyx.pojo;

import lombok.Data;

@Data
public class Person {
    private Integer uid;
    private String uname;
    private Integer age;
    private String jkm;
    private Integer area_no;

    @Override
    public String toString() {
        return uid+"\t"+uname+"\t"+age+"\t"+jkm+"\t"+area_no+"\t";
    }
}
