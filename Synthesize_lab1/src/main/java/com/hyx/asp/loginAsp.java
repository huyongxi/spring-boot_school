package com.hyx.asp;

import com.hyx.mapper.personMapper;
import com.hyx.pojo.Person;
import lombok.extern.log4j.Log4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Aspect
@Component
public class loginAsp {
    @Autowired
    personMapper personMapper;

    @Before("execution(* com.hyx.controller.loginController.login(*))")
    public void doBeforeAdvice(JoinPoint joinPoint){
        Person person = (Person) joinPoint.getArgs()[0];
        Scanner sc = new Scanner(System.in);
        System.out.print("健康码-key a y/n:");
        String s = sc.nextLine();
        person.setJkm(s);
        System.out.println("AOP 拦截成功");
    }

}
