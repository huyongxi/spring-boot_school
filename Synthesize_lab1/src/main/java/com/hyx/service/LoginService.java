package com.hyx.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hyx.mapper.areaMapper;
import com.hyx.mapper.personMapper;
import com.hyx.pojo.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class LoginService {

    @Autowired
    personMapper personMapper;
    @Autowired
    areaMapper areaMapper;

    public void addMessage(Person person) {
        personMapper.insert(person);
    }

    public String getMessage(Integer id) {
        QueryWrapper<Person> wrapper = new QueryWrapper<>();
        wrapper.eq("uid", id);
        Person person = personMapper.selectOne(wrapper);
        return person.toString() + areaMapper.selectBatchIds(Arrays.asList(person.getArea_no())).get(0);
    }

}
